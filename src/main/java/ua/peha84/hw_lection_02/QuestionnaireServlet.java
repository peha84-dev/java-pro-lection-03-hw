package ua.peha84.hw_lection_02;

import java.io.*;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

@WebServlet(name = "questionnaireServlet", value = "/questionnaire")
public class QuestionnaireServlet extends HttpServlet {

    public void init() {
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String a = request.getParameter("a");
        HttpSession session = request.getSession(false);

        if ("exit".equals(a) && (session != null)) {
            session.removeAttribute("q1");
            session.removeAttribute("q2");
        }

        response.sendRedirect("index.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String q1 = request.getParameter("q1");
        String q2 = request.getParameter("q2");

        HttpSession session = request.getSession(true);
        session.setAttribute("q1", q1);
        session.setAttribute("q2", q2);

        response.sendRedirect("index.jsp");
    }

    public void destroy() {
    }
}
