<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>JSP - Hello World</title>
</head>
<body>
<h1><%= "Please fill out the questionnaire." %>
</h1>
<br/>

<form action="./questionnaire" method="post">
    1) Do you love Java? <br/>
    <label>
        <input type="radio" name="q1" value="yes">
        Yes
    </label>  <br/>
    <label>
        <input type="radio" name="q1" value="no">
        No
    </label>  <br/>
    2) Do you love Python? <br/>
    <label>
        <input type="radio" name="q2" value="yes">
        Yes
    </label><br>
    <label>
        <input type="radio" name="q2" value="no">
        No
    </label><br>
    <input type="submit" name="submit" value="Send">
</form>

<% String q1 = (String)session.getAttribute("q1"); %>
<% String q2 = (String)session.getAttribute("q2"); %>

<% if (q1 == null || "".equals(q1) || q2 == null || "".equals(q2)) { %>
<% } else { %>
<br>
<br>
Your previous answers:
<table style="border: 1px solid black;border-collapse: collapse;">
    <tr>
        <td style="border: 1px solid black;border-collapse: collapse;"><strong>Do you love Java? </strong></td>
        <td style="border: 1px solid black;border-collapse: collapse;"><strong><%= q1 %></strong></td>
    </tr>
    <tr>
        <td style="border: 1px solid black;border-collapse: collapse;"><strong>Do you love Python? </strong></td>
        <td style="border: 1px solid black;border-collapse: collapse;"><strong><%= q2 %></strong></td>
    </tr>
</table>
<br>
<strong> <a href="./questionnaire?a=exit"> Click this link to clear previous answers. </a> </strong>
<% } %>

</body>
</html>